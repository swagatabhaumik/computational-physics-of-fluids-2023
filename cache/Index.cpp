//Index traverse
#include <iostream>
#include "Matrix.h"
#include <chrono>
#include <cmath>
#include <sys/time.h>

double mysecond()
{
        struct timeval tp;
        struct timezone tzp;
        int i;

        i = gettimeofday(&tp,&tzp);
        return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
}

typedef double myReal;

int main(int argc, char const *argv[])
{
	double time, sum=0.;
//    int N = 8000;
    std::cout<<"#N\tFasted index\tSlowest index"<<std::endl;   
    for(int N = 1000; N<=8000; N*=2){
    AMatrix<myReal> A(N,N);
    
    for (int i =0; i<N; i++)
        for (int j =0; j<N; j++)
            A(i,j) = drand48();

    time = mysecond();
    for (int i =0; i<N; i++)
        for (int j =0; j<N; j++)
            sum+=A(i,j);

    time = (mysecond() - time);
    std::cout<<N<<"\t"<<time<<"\t";   

    time = mysecond();

    for (int j=0; j<N; j++)
        for (int i=0; i<N; i++)
            sum+=A(i,j);
    time = (mysecond() - time);
    std::cout<<time<<std::endl;   
}
    if (sum>0)
        std::cout<<"#done"<<std::endl;
    

	return 0;
}

