#include <iostream>
#include "Matrix.h"
#include <chrono>
#include <cmath>
#include <sys/time.h>

double mysecond()
{
        struct timeval tp;
        struct timezone tzp;
        int i;

        i = gettimeofday(&tp,&tzp);
        return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
}

typedef double myReal;

int main(int argc, char const *argv[])
{
		myReal sum =0;

	double t;
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<myReal> dif(0.0);

	unsigned int N = 1e8, iterations = 10;

    AMatrix<myReal> A(1,N);
    AMatrix<myReal> B(1,N);
    AMatrix<myReal> C(1,N);
	for(int i=N-1; i>=0; i--){
            	A(0,i) = sin(1.*i);
            	B(0,i) = cos(1.*i);
            }
for(int stride=1; stride<128; stride+=4)
    {
//    	int stride =1;
//    start = std::chrono::system_clock::now();
    	t= mysecond();
	    	for(int i=N-128; i>=128; i-=stride){
//            	B(0,i) = A(0,i-stride)+A(0,i+stride)+A(0,i);
            	sum += A(0,i);
//    end = std::chrono::system_clock::now();
//    dif = end - start;
            }
    t = (mysecond() - t);

    std::cout<<"Stride value="<<stride<<"\t Time per Iteration in seconds: "<<t*stride<<std::endl;   

	std::cout<<sum;

	}

	return 0;
}

