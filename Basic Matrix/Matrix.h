//THE BASIC MATRIX CLASS
/*
1D alligned memory allocation for matrix
This class can be used upto 4D matrix
*/
#include <mm_malloc.h>

template <typename T> class AMatrix 
{
public:                                                                                     //public
    T * mat;
    unsigned int cols;
    unsigned int rows;
    unsigned int mols;
    unsigned int tols;
                                                                                            //Constructors
    AMatrix(unsigned int c, unsigned int r, unsigned int m, unsigned int t);
    AMatrix(unsigned int c, unsigned int r, unsigned int m);
    AMatrix(unsigned int c, unsigned int r);
    AMatrix(const AMatrix<T>& rhs);
    ~AMatrix();

                                                                                            //Operator overloading
    T& operator()(const unsigned int c, const unsigned int r, const unsigned int m, const unsigned int t)       { return mat[ c*rows*mols*tols + r*mols*tols + m*tols + t ];}
    T  operator()(const unsigned int c, const unsigned int r, const unsigned int m, const unsigned int t) const { return mat[ c*rows*mols*tols + r*mols*tols + m*tols + t ];}
    T& operator()(const unsigned int c, const unsigned int r, const unsigned int m)       { return mat[ c*rows*mols + r*mols + m ];}
    T  operator()(const unsigned int c, const unsigned int r, const unsigned int m) const { return mat[ c*rows*mols + r*mols + m ];}
    T& operator()(const unsigned int c, const unsigned int r)       { return mat[ c*rows + r ];}
    T  operator()(const unsigned int c, const unsigned int r) const { return mat[ c*rows + r ];}
};

//Constructors
template<typename T> AMatrix<T>::AMatrix(unsigned int c, unsigned int r, unsigned int m, unsigned int t) {
    mat=(T *) _mm_malloc(c*r*m*t*sizeof(T), 4096);
    cols = c;
    rows = r;
    mols = m;
    tols = t;
}

template<typename T> AMatrix<T>::AMatrix(unsigned int c, unsigned int r, unsigned int m) {
    mat=(T *) _mm_malloc(c*r*m*sizeof(T), 4096);
    cols = c;
    rows = r;
    mols = m;
    tols = 1;
}

template<typename T> AMatrix<T>::AMatrix(unsigned int c, unsigned int r) {
    mat=(T *) _mm_malloc(c*r*sizeof(T), 4096);
    cols = c;
    rows = r;
    mols = 1;
    tols = 1;
}

//Copy constructor
template<typename T> AMatrix<T>::AMatrix(const AMatrix<T>& rhs) {
     cols = rhs.cols;
     rows = rhs.rows;
     mols = rhs.mols;
     tols = rhs.tols;
     mat=rhs.mat;
}
//Destructor
template<typename T> AMatrix<T>::~AMatrix() {
    free(mat);
}
///Matrix Swap
template <typename T> void swapAMatrix(AMatrix<T>& A, AMatrix<T>& B){
    T * swap_mat;
    swap_mat = A.mat;
    unsigned int swap_cols = A.cols;
    unsigned int swap_rows = A.rows;
    unsigned int swap_mols = A.mols;
    unsigned int swap_tols = A.tols;

    A.mat = B.mat;
    A.cols = B.cols;
    A.rows = B.rows;
    A.mols = B.mols;
    A.tols = B.tols;

    B.mat = swap_mat;
    B.cols = swap_cols;
    B.rows = swap_rows;
    B.mols = swap_mols;
    B.tols = swap_tols;
}
