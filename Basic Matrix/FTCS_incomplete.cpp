#include <iostream>
#include <stdio.h>
#include <iomanip>
#include <math.h>
#include <stddef.h>
#include <cstdlib>

#include "Matrix.h"

typedef double myReal;
using namespace std;

void print(AMatrix<myReal>& field, int size);

int main(int argc, char const *argv[])
{
    int nX = 100;
    myReal factor = 1.;

    AMatrix<double> field(1,nX);
    AMatrix<double> fieldNew(1,nX);
    int iter = 0;
    while(iter<1000)
    {
    	for(int i=1; i<nX-1; i++)
            	fieldNew(0,i) = field(0,i) + factor*(field(0,i+1) + field(0,i-1)+2.*field(0,i));
	}
    print(field,nX);
	return 0;
}

void print(AMatrix<myReal>& field, int size)
{
    for(int i=0; i<size; i++)
    { 
	    cout<<field(0,i)<<endl;
    }
}