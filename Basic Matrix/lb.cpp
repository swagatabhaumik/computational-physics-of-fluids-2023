#include <iostream>
#include <stdio.h>
#include <iomanip>
#include <math.h>
#include <stddef.h>
#include <cstdlib>

#include "Matrix.h"

typedef double myReal;
using namespace std;

void print(AMatrix<myReal>& field, int size);

int main(int argc, char const *argv[])
{
    int nX = 100;
    int dv = 3;

    AMatrix<double> f(nX,dv);

    for(int i=0; i<nX; i++){
        f(i,0) = ;//f(-1) value at grid i
        f(i,1) = ;//f(0) value at grid i
        f(i,2) = ;//f(1) value at grid i
    }


	return 0;
}
